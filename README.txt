README file for Commerce Target2Sell

CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Configuration
* How it works
* Troubleshooting
* Maintainers

INTRODUCTION
------------
Drupal Commerce Integration with Target2Sell API.
This module helps to integrate a Drupal Commerce site with Target2Sell tools.
* Tracking on the website
  Target2Sell needs to know about the activities of the e-commerce website’s visitors to function.
  The data used by Target2Sell engine can be for instance:
  - referer whereby the visitor enters on your website
  - data related to the pages he visits
  - products and categories he watches or clicks on
  - products that he adds to cart
  - products he buys
  - etc.
* Catalog XML Export
  Target2Sell has to « know » the product catalog to work.
  This catalog has to contain the whole of website products, the adaptable products,
  the categories and caracteristics associated to each product.

REQUIREMENTS
------------
This module requires the following modules:
* Taxonomy (Drupal Core module)
* Commerce Cart (Drupal Commerce module)


INSTALLATION
------------
* Install as you would normally install a contributed drupal module.
  See:    https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.


CONFIGURATION
-------------
* Configure user permissions in Administration > People > Permissions:
   - Administer Commerce Target2Sell
     Users in roles with the "Administer Commerce Target2Sell" permission will
     be able to configure the website integration with Target2Sell.
* Configure the Commerce Target2Sell settings in
  Administration > Store > Configuration > Target2Sell
  Settings available:
  - Customer ID: The customer ID assigned by Target2Sell for this website;
  - User Roles [Tracking website]: Add tracking script for specific roles;
  - Content types [Tracking website]: Add tracking script for specific content types;
  - Category vocabulary [Catalog XML Export]: The category vocabulary for catalog export;
  - Product data mapping [Catalog XML Export]: Use tokens to map some products data for Catalog XML Export;


HOW IT WORKS
------------
* General considerations:
  - Shop owner must have a Target2Sell account
    http://www.target2sell.com/en/
* Tracking on the website:
  The module adds a Tracking script required by Target2Sell
* Catalog XML Export
  The module builds an XML file to be used/read by Target2Sell which.
* API available
  The module will not provide "out of box" solution for Target2Sell integration
  in every Drupal Commerce sites.
  For both integrations, there are API available to help Store developers to implement
  solution for custom Drupal Commerce Stores


TROUBLESHOOTING
---------------
ToDo


MAINTAINERS
-----------
Current maintainers:
* Tavi Toporjinschi (vasike) - https://www.drupal.org/u/vasike

This project has been developed by:
* Commerce Guys by Actualys
  Visit https://commerceguys.fr/en for more information.
