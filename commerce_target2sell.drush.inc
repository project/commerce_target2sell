<?php

/**
 * @file
 * Drush commands for Commerce Target2Sell.
 */

/**
 * Implements hook_drush_command().
 */
function commerce_target2sell_drush_command() {
  $items = array();

  $items['commerce-target2sell-catalog-export'] = array(
    'description' => 'Export products data for Target2Sell Catalog XML Export.',
    'examples' => array(
      'drush commerce-target2sell-catalog-export' => dt('Export products data for Target2Sell Catalog XML Export.'),
      'drush ct2s-ce 10' => dt("Export products data (!batch_size items per batch run) for Target2Sell Catalog XML Export.", array('!batch_size' => 10)),
    ),
    'arguments' => array(
      'batch_size' => dt("The number of items to export per batch run. 20 is the batch size by default."),
    ),
    'aliases' => array('ct2s-ce'),
  );

  $items['commerce-target2sell-catalog-export-clear'] = array(
    'description' => 'Clear all products data for Target2Sell Catalog XML Export.',
    'examples' => array(
      'drush ct2s-ce-c' => dt('Clear all products data for Target2Sell Catalog XML Export.'),
    ),
    'aliases' => array('ct2s-ce-c'),
  );

  return $items;
}

/**
 * Export catalog products data for Target2Sell Catalog XML.
 *
 * @param integer $batch_size
 *   Number of products to export per batch.
 */
function drush_commerce_target2sell_catalog_export($batch_size = 20) {
  $process_batch = FALSE;
  if (drush_commerce_target2sell_export_catalog_products_data_update($batch_size)) {
    $process_batch = TRUE;
  }

  if ($process_batch) {
    drush_backend_batch_process();
  }
}

/**
 * Creates and sets a batch for exporting products for Target2Sell catalog.
 * @param int $batch_size
 *   (optional) The number of items to export per batch. 20 by default.
 *
 * @return bool
 *   TRUE if batch was created, FALSE otherwise.
 */
function drush_commerce_target2sell_export_catalog_products_data_update($batch_size = 20) {
  drush_log(dt("Exporting products data (!batch_size items per batch run) for Target2Sell Catalog XML Export.", array('!batch_size' => $batch_size)), 'ok');

  // Create the batch.
  if (!commerce_target2sell_export_catalog_products_data_update($batch_size, array('drush' =>TRUE))) {
    drush_log(dt("Couldn't create a batch, something was wrong."), 'error');
    return FALSE;
  }

  return TRUE;
}

/**
 * Clear all products data for Target2Sell Catalog XML Export.
 */
function drush_commerce_target2sell_catalog_export_clear() {
  commerce_target2sell_export_catalog_product_data_clear_all();
  drush_log(dt('Products data for Target2Sell Catalog XML Export cleared.'), 'ok');
}