<?php

/**
 * @file
 * Hooks provided by the Commerce Target2Sell module.
 */

/**
 * Lets modules alter Target2Sell tracking data right before building the
 * Target2Sell tracking script.
 *
 * @param &$t2s_tracking_data
 *   The existing array of tracking data to be altered.
 *
 * @see commerce_target2sell_page_alter()
 */
function hook_commerce_target2sell_tracking_data_alter(&$t2s_tracking_data) {
  // Example.
  $t2s_tracking_data['domain'] = 'example.com';
  $t2s_tracking_data['wl'] = custom_module_target2sell_products_wishlist();
}

/**
 * Custom build of Target2Sell products data for Catalog XML Export.
 *
 * For some Drupal Commerce Sites this could be a quick solution to get the
 * custom products data for Target2Sell Catalog XML Export.
 * If this hook is implemented no more product data will be built by this
 * module.
 *
 * @param $product_display
 *   The product display node object to build data for.
 *
 * @see commerce_target2sell_export_catalog_product_data_build.
 * @see target2sell-export-catalog-product.tpl.php
 */
function hook_commerce_target2sell_export_catalog_product_data($product_display) {
  $wrapper = entity_metadata_wrapper('node', $product_display);
  // Build the product data.
  $product_data = array();
  if (empty($product_display->products)) {
    return $product_data;
  }
  $product_data['product_id'] = $product_display->nid;
  $product_data['sku'] = $wrapper->products[0]->sku->value();

  // And so on ... according with Catalog XML Export Target2Sell documentation.

}

/**
 * Lets modules alter Target2Sell product data for Catalog XML Export.
 *
 * This implementation is helpful when you need only small customizations for
 * product data besides what the module already provides.
 *
 * @param &$product_data
 *   The existing array of product data to be altered.
 * @param $product_display
 *   The product display node object to build data for.
 *
 * @see commerce_target2sell_export_catalog_product_data_build.
 * @see target2sell-export-catalog-product.tpl.php
 */
function hook_commerce_target2sell_export_catalog_product_data_alter(&$product_data, $product_display) {
  // Example.
  $wrapper = entity_metadata_wrapper('node', $product_display);
  // Some custom data.
  $product_data['long_desc'] = $wrapper->products[0]->field_custom_long_desc->value();
  $product_data['stock_qty'] = $wrapper->products[0]->field_custom_stock->value();
  // Custom callback to build product attributes data.
  $product_data['attributes'] = custom_module_target2sell_export_catalog_product_attributes();

  // And so on ... according with Catalog XML Export Target2Sell documentation.

}
