<?php
/**
 * @file
 * ToDo: variables available
 *
 * @see commerce_target2sell_export_catalog_product_data_build()
 * @see template_preprocess_target2sell_export_catalog_category
 *
 * @ingroup themeable
 */
?>
<category id="<?php print $category['id']; ?>">
  <parent_id><?php print $category['parent_id']; ?></parent_id>
  <is_active><?php print $category['active']; ?></is_active>
  <name><![CDATA[<?php print $category['name']; ?>]]></name>
</category>
