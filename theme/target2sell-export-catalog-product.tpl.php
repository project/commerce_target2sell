<?php
/**
 * @file
 * ToDo: variables available
 *
 * @see commerce_target2sell_export_catalog_product_data_build()
 * @see template_preprocess_target2sell_export_catalog_product()
 *
 * @ingroup themeable
 */
?>
<product id="<?php print $product_id; ?>">
  <sku><?php print $sku; ?>></sku>
  <name><![CDATA[<?php print $name; ?>]]></name>
  <short_desc><![CDATA[<?php print $short_desc; ?>]]></short_desc>
  <long_desc><![CDATA[<?php print $long_desc; ?>]]></long_desc>
  <url><![CDATA[<?php print $url; ?>]]></url>
  <?php if (!empty($img_url)) : ?>
  <img_url><![CDATA[<?php print $img_url; ?>]]></img_url>
  <?php endif; ?>
  <price><?php print $price; ?></price>
  <?php if (isset($salable)) : ?>
  <salable><?php print $salable; ?></salable>
  <?php endif; ?>
  <?php if (isset($in_stock)) : ?>
  <in_stock><?php print $in_stock; ?></in_stock>
  <?php endif; ?>
  <?php if (!empty($stock_qty)) : ?>
  <stock_qty><?php print $stock_qty; ?></stock_qty>
  <?php endif; ?>
  <?php if (!empty($categories)) : ?>
  <categories>
    <?php foreach ($categories as $category) : ?>
      <id><?php print $category; ?></id>
    <?php endforeach; ?>
  </categories>
  <?php endif; ?>
  <declinations>
    <?php if (!empty($declinations)) : ?>
    <?php foreach ($declinations as $declination) : ?>
      <declination id="<?php print $declination['id']; ?>" price="<?php print $declination['price']; ?>">
        <?php if (!empty($declination['attributes'])) : ?>
          <?php foreach ($declination['attributes'] as $attribute) : ?>
          <attribute id="<?php print $attribute['id']; ?>" name="<?php print $attribute['name']; ?>" type="<?php print $attribute['type']; ?>">
            <value id="<?php print $attribute['value_id']; ?>"><![CDATA[<?php print $attribute['value']; ?>]]></value>
          </attribute>
          <?php endforeach; ?>
        <?php endif; ?>
      </declination>
    <?php endforeach; ?>
    <?php endif; ?>
  </declinations>
  <attributes>
    <?php if (!empty($product_attributes)) : ?>
    <?php foreach ($product_attributes as $attribute) : ?>
      <attribute id="<?php print $attribute['id']; ?>" name="<?php print $attribute['name']; ?>" type="<?php print $attribute['type']; ?>">
        <?php if (!empty($attribute['values'])) : ?>
        <?php foreach ($attribute['values'] as $value) : ?>
          <value id="<?php print $value['id']; ?>"><![CDATA[<?php print $value['value']; ?>]]></value>
        <?php endforeach; ?>
        <?php endif; ?>
      </attribute>
    <?php endforeach; ?>
    <?php endif; ?>
  </attributes>
  <?php if (!empty($sets)) : ?>
    <sets>
      <?php foreach ($sets as $set) : ?>
        <?php if (!(empty($set['price']) && !is_numeric($value))) : ?>
        <set id="<?php print $set['id']; ?>"<?php print $set['default']; ?>>
          <price><?php print $set['price']; ?></price>
          <?php if (!empty($set['stock'])) : ?>
          <stock><?php print $set['stock']; ?></stock>
          <?php endif; ?>
        </set>
        <?php endif; ?>
      <?php endforeach; ?>
    </sets>
  <?php endif; ?>
</product>
