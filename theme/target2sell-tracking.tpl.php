<?php
/**
 * @file
 * ToDo: variables available
 *
 * @see commerce_target2sell_tracking_build_script()
 *
 * @ingroup themeable
 */
?>
<script type="text/javascript">
  //<![CDATA[
  var _t2sparams = _t2sparams || {};
  <?php
    foreach ($params as $key => $value) {
  ?>
  _t2sparams.<?php print $key; ?> = '<?php print $value; ?>';
  <?php
    }
  ?>
  (function() {
    var t2sScript = document.createElement('script');
    t2sScript.type = 'text/javascript';
    t2sScript.async = true;
    t2sScript.src = ('https:' ==document.location.protocol ?
      'https://static.target2sell.com/t2s.min.js' :
      'http://s3.target2sell.com/t2s.min.js');
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(t2sScript, s);
    t2sScript.onload = function() {
      T2S.reco();
    }
  })();
  //]]>
</script>
