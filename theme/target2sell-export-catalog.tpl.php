<?php
/**
 * @file
 * ToDo: variables available
 *
 * @see commerce_target2sell_export_catalog_product_data_build()
 * @see template_preprocess_target2sell_export_catalog()
 *
 * @ingroup themeable
 */
?>
<?php
  // It requires to use php, do avoid a php error;
  print '<?xml version="1.0" encoding="utf-8"?>';
?>

  <catalog xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="target2sell-catalog-v1.0.xsd">
    <price_format currency_symbol="€" dec_sep="," k_sep=" "><![CDATA[9 999,99 €]]></price_format>
    <?php if (!empty($categories)) : ?>
    <categories size="<?php print count($categories);?>">
      <?php foreach ($categories as $category) : ?>
        <?php print $category; ?>
      <?php endforeach; ?>
    </categories>
    <?php endif; ?>
    <?php if (!empty($products)) : ?>
    <products size="<?php print count($products);?>">
      <?php foreach ($products as $product) : ?>
        <?php print $product; ?>
      <?php endforeach; ?>
    </products>
    <?php endif; ?>
  </catalog>