<?php

/**
 * @file
 * Preprocessors and helper functions to make theming easier.
 */

/**
 * Preprocess the primary theme implementation for a view.
 */
function template_preprocess_target2sell_export_catalog(&$vars) {
}

/**
 * Preprocess the primary theme implementation for a view.
 */
function template_preprocess_target2sell_export_catalog_category(&$vars) {
}

/**
 * Preprocess the primary theme implementation for a view.
 */
function template_preprocess_target2sell_export_catalog_product(&$vars) {
  if (!empty($vars['product_data']) && is_array($vars['product_data'])) {
    $vars += $vars['product_data'];
    unset($vars['product_data']);
  }
}