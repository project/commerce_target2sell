<?php

/**
 * @file
 * Contains the administrative page and form callbacks.
 */

/**
 * Form constructor for the module settings form.
 *
 * @see commerce_target2sell_settings_form_validate()
 *
 * @ingroup forms
 */
function commerce_target2sell_settings_form($form, &$form_state) {
  // Gather data.
  // @todo Set this on validation errors.
  $default_tab = !empty($form_state['default_tab']) ? $form_state['default_tab'] : '';

  // Build form elements.
  $form['tabs'] = array(
    '#type' => 'vertical_tabs',
    '#default_tab' => $default_tab ? $default_tab : 'edit-general',
  );

  $form['tabs'] = array(
    '#type' => 'vertical_tabs',
    '#default_tab' => $default_tab ? $default_tab : 'edit-general',
  );

  // General settings.
  $text = t('request a Target2Sell demo');
  $link = l($text, 'http://www.target2sell.com/en/request-demo/');

  // Build form elements.
  $general_tab = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
    '#tree' => FALSE,
  );

  $general_tab['commerce_target2sell_customer_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Customer ID'),
    '#description' => t('The customer ID assigned by Target2Sell for this website. To get a CUSTOMER ID, !link.', array('!link' => $link)),
    '#default_value' => variable_get('commerce_target2sell_customer_id', ''),
    '#attributes' => array('placeholder' => array('<YOUR_CUSTOMER_ID>')),
    '#size' => 20,
    '#maxlength' => 20,
    '#required' => TRUE,
  );
  $form['tabs']['general'] = $general_tab;

  //
  $tracking_tab = array(
    '#type' => 'fieldset',
    '#title' => t('Tracking website'),
    '#description' => t('Tracking website settings.'),
    '#tree' => FALSE,
  );
  // Build form elements.
  $tracking_role_toogle = variable_get('commerce_target2sell_tracking_role_toggle', 0);
  $tracking_role_list = variable_get('commerce_target2sell_tracking_role_list', 0);
  $tracking_tab['roles'] = array(
    '#type' => 'fieldset',
    '#title' => t('User roles'),
    '#description' => t('On this tab, specify the user role condition.'),
    '#collapsible' => TRUE,
    '#collapsed' => (empty($tracking_role_toogle) && empty($tracking_role_list)) ? TRUE : FALSE,
    '#tree' => FALSE,
  );

  $roles = array_map('check_plain', user_roles());
  $tracking_tab['roles']['commerce_target2sell_tracking_role_toggle'] = array(
    '#type' => 'radios',
    '#title' => t('Add tracking script for specific roles'),
    '#options' => array(
      t('All roles except the selected roles'),
      t('Only the selected roles'),
    ),
    '#default_value' => $tracking_role_toogle,
  );

  $tracking_tab['roles']['commerce_target2sell_tracking_role_list'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Selected roles'),
    '#default_value' => $tracking_role_list,
    '#options' => $roles,
  );

  $tracking_node_types = array_filter(variable_get('commerce_target2sell_tracking_node_types', array()));
  $tracking_tab['node_type'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content types'),
    '#collapsible' => TRUE,
    '#collapsed' => empty($tracking_node_types) ? TRUE : FALSE,
  );
  $tracking_tab['node_type']['commerce_target2sell_tracking_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Add tracking script for specific content types'),
    '#default_value' => $tracking_node_types,
    '#options' => node_type_get_names(),
    '#description' => t('Add tracking script only on pages that display content of the given type(s). If you select no types, there will be no type-specific limitation.'),
  );

  $form['tabs']['tracking'] = $tracking_tab;

  $export_catalog_tab = array(
    '#type' => 'fieldset',
    '#title' => t('Catalog XML Export'),
    '#description' => t('Catalog XML Export settings. Please use tokens if available. If not, you need to build the values using the API available.'),
    '#tree' => FALSE,
  );
  $vocabularies = taxonomy_get_vocabularies();
  $options = array();
  foreach ($vocabularies as $vocabulary) {
    $options[$vocabulary->machine_name] = $vocabulary->name;
  }
  $export_catalog_tab['commerce_target2sell_export_catalog_category'] = array(
    '#type' => 'select',
    '#title' => t('Category vocabulary'),
    '#default_value' => variable_get('commerce_target2sell_export_catalog_category', ''),
    '#options' => $options,
    '#required' => TRUE,
    '#description' => t('The category vocabulary for catalog export.'),
  );

  $export_catalog_tab['submit'] = array(
    '#type' => 'submit',
    '#limit_validation_errors' => array(),
    '#value' => t('Update export catalog product data.'),
    '#submit' => array('commerce_target2sell_export_catalog_products_data_update_submit'),
  );
  $export_catalog_tab['product_data'] = array(
    '#type' => 'fieldset',
    '#title' => t('Product data mapping'),
    '#description' => t('There tokens API could be used to build the XML. But custom code will be needed for sure.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => FALSE,
  );
  $export_catalog_tab_product = &$export_catalog_tab['product_data'];

  $export_catalog_tab_product['commerce_target2sell_export_catalog_product_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Product ID'),
    '#description' => t('The identifier to be used for product id. If not set, product display node id will be used'),
    '#default_value' => variable_get('commerce_target2sell_export_catalog_product_id', ''),
    '#size' => 40,
  );
  $export_catalog_tab_product['commerce_target2sell_export_catalog_product_sku'] = array(
    '#type' => 'textfield',
    '#title' => t('Product SKU'),
    '#description' => t('The SKU to be used for main product. If not set, the default product sku will be used.'),
    '#default_value' => variable_get('commerce_target2sell_export_catalog_product_sku', ''),
    '#size' => 40,
  );
  $export_catalog_tab_product['commerce_target2sell_export_catalog_product_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Product name'),
    '#description' => t('The name to be used for main product. If not set, the product display title will be used.'),
    '#default_value' => variable_get('commerce_target2sell_export_catalog_product_name', ''),
    '#size' => 40,
  );
  $export_catalog_tab_product['commerce_target2sell_export_catalog_product_long_desc'] = array(
    '#type' => 'textfield',
    '#title' => t('Long description'),
    '#description' => t('Long description to be used for main product. If not set, the empty value will be used.'),
    '#default_value' => variable_get('commerce_target2sell_export_catalog_product_long_desc', ''),
    '#size' => 40,
  );
  $export_catalog_tab_product['commerce_target2sell_export_catalog_product_short_desc'] = array(
    '#type' => 'textfield',
    '#title' => t('Short description'),
    '#description' => t('Short description to be used for main product. If not set, the empty value will be used.'),
    '#default_value' => variable_get('commerce_target2sell_export_catalog_product_short_desc', ''),
    '#size' => 40,
  );
  $export_catalog_tab_product['commerce_target2sell_export_catalog_product_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Product URL'),
    '#description' => t('The url for product. If not set, product display node url will be used'),
    '#default_value' => variable_get('commerce_target2sell_export_catalog_product_url', ''),
    '#size' => 40,
  );
  $export_catalog_tab_product['commerce_target2sell_export_catalog_product_img_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Image URL'),
    '#description' => t('The main image url for product. If not set or built within code API, the empty value will be used.'),
    '#default_value' => variable_get('commerce_target2sell_export_catalog_product_img_url', ''),
    '#size' => 40,
  );
  $export_catalog_tab_product['commerce_target2sell_export_catalog_product_price'] = array(
    '#type' => 'textfield',
    '#title' => t('Price amount'),
    '#description' => t('The price amount for product. If not set or built within code API, the default product price amount will be used.'),
    '#default_value' => variable_get('commerce_target2sell_export_catalog_product_price', ''),
    '#size' => 40,
  );
  $export_catalog_tab_product['commerce_target2sell_export_catalog_product_promotion'] = array(
    '#type' => 'textfield',
    '#title' => t('Promotion amount'),
    '#description' => t('The promotion amount for product if any. If not set or built within code API, the empty value will be used.'),
    '#default_value' => variable_get('commerce_target2sell_export_catalog_product_promotion', ''),
    '#size' => 40,
  );
  $export_catalog_tab_product['commerce_target2sell_export_catalog_product_salable'] = array(
    '#type' => 'textfield',
    '#title' => t('Product status'),
    '#description' => t('The value (1 or 0) if the product can be sold or not. If not set or built within code API, the default product status will be used.'),
    '#default_value' => variable_get('commerce_target2sell_export_catalog_product_salable', ''),
    '#size' => 40,
  );
  $export_catalog_tab_product['commerce_target2sell_export_catalog_product_in_stock'] = array(
    '#type' => 'textfield',
    '#title' => t('In stock'),
    '#description' => t('The value (1 or 0) if the product is available or not.'),
    '#default_value' => variable_get('commerce_target2sell_export_catalog_product_in_stock', ''),
    '#size' => 40,
  );
  $export_catalog_tab_product['commerce_target2sell_export_catalog_product_stock_qty'] = array(
    '#type' => 'textfield',
    '#title' => t('Stock quantity'),
    '#description' => t('Number of available products. If not set or built within code API, the empty value will be used.'),
    '#default_value' => variable_get('commerce_target2sell_export_catalog_product_stock_qty', ''),
    '#size' => 40,
  );
  $export_catalog_tab_product['commerce_target2sell_export_catalog_product_categories'] = array(
    '#type' => 'textfield',
    '#title' => t('Categories'),
    '#description' => t('Categories related to the product. Only the lowest level category. This requires coding for sure and it should respect the expected format - see template file.'),
    '#default_value' => variable_get('commerce_target2sell_export_catalog_product_categories', ''),
    '#size' => 40,
  );
  $export_catalog_tab_product['commerce_target2sell_export_catalog_product_declinations'] = array(
    '#type' => 'textfield',
    '#title' => t('Declinations - product variations.'),
    '#description' => t('Data for product variations. This requires coding for sure and it should respect the expected format - see template file.'),
    '#default_value' => variable_get('commerce_target2sell_export_catalog_product_declinations', ''),
    '#size' => 40,
  );
  $export_catalog_tab_product['commerce_target2sell_export_catalog_product_attributes'] = array(
    '#type' => 'textfield',
    '#title' => t('Attributes - product data.'),
    '#description' => t('Data for main product. This requires coding for sure and it should respect the expected format - see template file.'),
    '#default_value' => variable_get('commerce_target2sell_export_catalog_product_attributes', ''),
    '#size' => 40,
  );

  // Token tree display STARTS

  if (module_exists('token2')) {
    $export_catalog_tab['token_tree'] = array(
      '#theme' => 'token_tree',
      '#token_types' => array('node'),
      '#global_types' => FALSE,
      //'#dialog' => TRUE,
    );
  }
  else {
    $export_catalog_tab['token_tree'] = array(
      '#markup' => '' . t('Enable the Token module to view the available token browser.', array('@drupal-token' => 'http://drupal.org/project/token')) . '',
    );
  }

  $form['tabs']['export_catalog'] = $export_catalog_tab;

  return system_settings_form($form);
}

/**
 * Form validation handler for commerce_target2sell_settings_form().
 */
function commerce_target2sell_settings_form_validate($form, &$form_state) {
  $values = &$form_state['values'];

  // Trim the text values.
  $values['commerce_target2sell_customer_id'] = trim($values['commerce_target2sell_customer_id']);
}

/**
 * Form Submit handler for "Update export catalog product data" button.
 */
function commerce_target2sell_export_catalog_products_data_update_submit($form, $form_state) {
  // Call the batch function for exporting products data for Target2Sell Catalog XML Export.
  commerce_target2sell_export_catalog_products_data_update();
}